# GitLab milestone dashboard

This app was created with the intention to replace the blackboard setup I had in my home office.
At the start of each milestone I would copy down all the issues I had assigned to me for that milestone and update them every day with the current status.
Not wanting to be a human API anymore, and wanting to boost my graphql knowledge, I decided to create vue app to replace myself.

Here's how it works:
1. Load up the app, which is currently at https://samdbeckham.gitlab.io/milestone-dashboard.
2. Hit that login button to log in with your GitLab.com account. If you're already logged in to GitLab.com, you should be asked to authorize the app. If not, you'll need to log in to GitLab first, then authorize the app.
3. Profit[.](https://en.wikipedia.org/wiki/Gnomes_(South_Park))

Once you're on your dashboard you'll see the current milestone along with a list of all the issues you have assigned to you for that milestone.
There's also some counters at the top, but they're still a little in flux so I'll gloss over them for now.

Right in the center of the app is a progress counter with two rings.
The outer ring counts the elapsed working days in the current milestone.
The inner ring shows your progress through the milestone which is calculated using the various labels and statuses on the issues assigned to you.
If you're on schedule, it's green. If you're behind, it's orange.
It's not the most accurate, but we aim to improve it over time and it's [good enough for now](https://about.gitlab.com/handbook/values/#boring-solutions).


## Developing on the project
In the true spirit of [everyone can contribute](https://about.gitlab.com/company/strategy/#mission), this project is totally public.
There are some issues you can pick up, you can raise your own, or (better yet) make a merge request with the change you'd like to see.
Be sure to check out the [contributing guide](./CONTRIBUTING.md) before doing so.

### Project setup
```
npm install
```

#### Compiles and hot-reloads for development
```
npm run serve
```

#### Compiles and minifies for production
```
npm run build
```

#### Run your tests
```
npm run test
```

#### Lints and fixes files
```
npm run lint
```

#### Run your end-to-end tests
```
npm run test:e2e
```

#### Run your unit tests
```
npm run test:unit
```

#### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
