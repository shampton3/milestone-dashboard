import Vue from "vue";
import VueRouter from "vue-router";
import LandingPage from "@/views/landing_page.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Landing",
    component: LandingPage,
  },
  {
    path: "/dashboard/:username",
    name: "Dashboard",
    component: () =>
      import(/* webpackChunkName: "dashboard" */ "../views/dashboard_page.vue"),
  },
  {
    path: "/access_token=:token",
    name: "Login",
    component: () =>
      import(/* webpackChunkName: "login" */ "../views/login_page.vue"),
  },
];

const router = new VueRouter({
  routes,
});

export default router;
