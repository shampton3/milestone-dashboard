# Contributing
So, you wanna contribute to this project huh?
Please read the following before doing so.

## Setting up your dev environment
Please see the [project setup](./README.md#project-setup) section of the readme for help.

## Style Guide
We don't want to re-invent the wheel with the style guide so we use [gitlab-eslint-config](https://gitlab.com/gitlab-org/gitlab-eslint-config) along with [prettier](https://prettier.io/) to do all the heavy lifting.
You can lint all your files with `npm run lint` or set prettier up to format on save.
If you go against convention, the CI job will fail when you push up your merge request and it won't get merged.

## Testing
We have unit testing and end-to-end testing on this project.
You can run them using `npm run test:unit` and `npm run test:e2e` respectively.
If you're adding new functionality, please write tests for it.
If you're changing existing functionality, please update the relevant tests.
If you're fixing a bug that the current tests missed, please add a test case to catch it.

## Raising an issue
If you want to fix a bug, create a small change, or add some functionality, then please [start with a merge request](https://about.gitlab.com/handbook/communication/#everything-starts-with-a-merge-request).
However, if you want to add a larger feature, or it something you can't do yourself, then please create an issue.
We have no real guidelines for this other than, "be descriptive".
The more descriptive you are, the better we can help.

## Did we miss anything?
If this document didn't have what you were looking for, then please [raise an issue](#raising-an-issue) and we'll get it added.
